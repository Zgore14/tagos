//
//  SavedTags+CoreDataProperties.swift
//  Tagos
//
//  Created by Kilian Lebrun on 2018-12-05.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//
//

import Foundation
import CoreData


extension SavedTags {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SavedTags> {
        return NSFetchRequest<SavedTags>(entityName: "SavedTags")
    }

    @NSManaged public var name: String?
    @NSManaged public var associatedPicture: NSObject?

}
