//
//  SavedPicture+CoreDataProperties.swift
//  Tagos
//
//  Created by Kilian Lebrun on 2018-12-05.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//
//

import Foundation
import CoreData


extension SavedPicture {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SavedPicture> {
        return NSFetchRequest<SavedPicture>(entityName: "SavedPicture")
    }

    @NSManaged public var dateOfCreation: NSDate?
    @NSManaged public var id: UUID?
    @NSManaged public var image: NSData?
    @NSManaged public var tags: NSObject?

}
