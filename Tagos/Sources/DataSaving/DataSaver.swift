//
//  DataSaver.swift
//  Tagos
//
//  Created by Kilian Lebrun on 2018-11-12.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataSaver {

    var _tagSave : [SavedTags] = []
    var _pictureSave : [SavedPicture] = []

    init() {
    }
    
    // MARK: Save a picture into CoreData (Use DataManager to Save)
    func SavePicture(iPicture: Picture) {
        
        // Create local picture to save
        let lSavingPicture = SavedPicture(context: PersistentService.context)
        lSavingPicture.image = iPicture._image.pngData()! as NSData
        lSavingPicture.id = iPicture._id
        lSavingPicture.dateOfCreation = iPicture._dateOfCreation as NSDate?
        lSavingPicture.tags = iPicture._tags as NSObject
        
        // Save the Picture
        PersistentService.saveContext()
    }
    
    // MARK: Save a Tag into CoreData
    func SaveTags(iTag: Tag) {
        
        // Create local tag to save (Use DataManager to Save)
        let lSavingTag = SavedTags(context: PersistentService.context)
        lSavingTag.name = iTag._name
        lSavingTag.associatedPicture = iTag._assiocatedPicture as NSObject
        
        // Save the tag
        PersistentService.saveContext()
    }
    
    // MARK: Modify a Tag
    func ModifyTag(iTag: Tag) {
        
        let lModifTagID = _tagSave.index(where: {(elem) -> Bool in elem.name == iTag._name})
        
        _tagSave[lModifTagID!].associatedPicture = iTag._assiocatedPicture as NSObject
        
        PersistentService.saveContext()
    }
    
    // MARK: Modify a Picture
    func ModifyPicture(iPicture: Picture) {
        let lModifPictureID = _pictureSave.index(where: {(elem) -> Bool in elem.id == iPicture._id})
        if lModifPictureID == nil {
            return
        }
        _pictureSave[lModifPictureID!].tags = iPicture._tags as NSObject
        
        PersistentService.saveContext()
    }
    
    // MARK: Delete all Pictures and Tags
    func Delete() {
        for item in _tagSave {
            PersistentService.context.delete(item);
        }
        for item in _pictureSave {
            PersistentService.context.delete(item)
        }
        PersistentService.saveContext()
    }
    
    // MARK: Delete a Picture.
    func DeletePicture(iPicture: Picture) {
        for item in _tagSave {
            var assocPic = item.associatedPicture as! [UUID]
            let lModifPictureinTagID = assocPic.index(where: {(elem) -> Bool in elem == iPicture._id })
            assocPic.remove(at: lModifPictureinTagID!)
        }
        
        let lModifPictureID = _pictureSave.index(where: {(elem) -> Bool in elem.id == iPicture._id})
        
        PersistentService.context.delete(_pictureSave[lModifPictureID!])
        
        PersistentService.saveContext()
    }
    
    
    // MARK: Delete a Tag. Use it only if you want to delete a Tag. If you want to modify a tag from a picture, use ModifyPicture instead.
    func DeleteTag(iTag: Tag) {
        for item in _pictureSave {
            var tags = item.tags as! [String]
            let lModifTaginPictureID = tags.index(where: {(elem) -> Bool in elem == iTag._name })
            tags.remove(at: lModifTaginPictureID!)
        }
        
        let lModifTagID = _tagSave.index(where: {(elem) -> Bool in elem.name == iTag._name })
        
        PersistentService.context.delete(_pictureSave[lModifTagID!])
        
        PersistentService.saveContext()
    }
    
    // MARK: Retrieve all pictures from DataCore
    func RetrievePictures() -> [Picture] {
        
        // Prepare fetchRequest in DataCore
        let fetchRequest: NSFetchRequest<SavedPicture> = SavedPicture.fetchRequest()
        var picturesRetrieve = [Picture]()
        do {
            let pic = try PersistentService.context.fetch(fetchRequest)
            for item in pic {
                let picture = Picture()
                picture._image = UIImage(data: item.image! as Data)!
                picture._image = UIImage(cgImage: picture._image.cgImage!, scale: picture._image.scale, orientation: .right)
                picture._id = item.id!
                picture._dateOfCreation = item.dateOfCreation! as Date
                picture._tags = item.tags as! [String]
                picture._saved = true
                picturesRetrieve.append(picture)
            }
        } catch {
            print("ERROR : Retrieving data's picture is impossible.")
        }
        return picturesRetrieve
    }
    
    // MARK: Retrieve all tags from DataCore
    func RetrieveTags() -> [Tag] {
        
        // Prepare fetchRequest in DataCore
        let fetchRequest: NSFetchRequest<SavedTags> = SavedTags.fetchRequest()
        var tagsRetrieve = [Tag]()
        do {
            let tagSaved = try PersistentService.context.fetch(fetchRequest)
            _tagSave = tagSaved
            for item in tagSaved {
                let tag = Tag()
                tag._name = item.name!
                tag._assiocatedPicture = item.associatedPicture as! [UUID]
                tagsRetrieve.append(tag)
                
            }
        } catch {
            print("ERROR : Retrieving data's tag is impossible.")
        }
        return tagsRetrieve
    }
}
