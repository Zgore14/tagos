//
//  Tags.swift
//  Tagos
//
//  Created by Kilian Lebrun on 2018-11-15.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import Foundation
import CoreData

class Tag {
    var _name : String
    var _assiocatedPicture : [UUID]
    
    init() {
        _name = String()
        _assiocatedPicture = [UUID]()
    }
    
    init(name: String) {
        _name = name
        _assiocatedPicture = [UUID]()
    }
    
    func addAssiocatedPicture(id : UUID) {
        _assiocatedPicture.append(id)
    }
}
