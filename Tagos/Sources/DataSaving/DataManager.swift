//
//  DataManager.swift
//  Tagos
//
//  Created by Kilian Lebrun on 2018-11-15.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import Foundation
import UIKit

struct GdataManager {
    static var dataManager = DataManager()
}

// MARK: Manage all datas. Save, Modify and Delete datas
class DataManager {
    var _dataSaver = DataSaver()
    var _pictures : [Picture]
    var _tags : [Tag]
    
    init() {
        _pictures = [Picture]()
        _tags = [Tag]()
    }
    
    init(iPictures : [Picture], iTags : [Tag]) {
        _pictures = iPictures
        _tags = iTags
    }
    
    // MARK: Clear Datas of pictures and tags
    private func ClearDatas() {
        _pictures.removeAll()
        _tags.removeAll()
    }
    
    // MARK: Recover Datas from CoreData
    func PullDatas() {
        _pictures = _dataSaver.RetrievePictures()
        _tags = _dataSaver.RetrieveTags()
    }
    
    // MARK:  Save an image with its tags as a Picture object
    func SavePicture(iImage : UIImage, iTags : [String]) -> Void {
        let picture = Picture(image: iImage, tags: iTags)
        var tagExist = false
        
        for tag in picture._tags {
            for tagAlreadyExist in _tags {
                if (tag == tagAlreadyExist._name) {
                    tagExist = true
                    tagAlreadyExist.addAssiocatedPicture(id: picture._id)
                    _dataSaver.ModifyTag(iTag: tagAlreadyExist)
                    break
                }
            }
            if (!tagExist) {
                let lTag = Tag(name: tag)
                lTag.addAssiocatedPicture(id: picture._id)
                _dataSaver.SaveTags(iTag: lTag)
            }
        }
        
        picture._saved = true
        
        _dataSaver.SavePicture(iPicture: picture)
        
        ClearDatas()
        PullDatas()
    }
    
   
    // MARK: Save a picture previously created
    func SavePicture(iPicture : Picture) -> Void {
        var tagExist = false
        
        for tag in iPicture._tags {
            for tagAlreadyExist in _tags {
                if (tag == tagAlreadyExist._name) {
                    tagExist = true
                    tagAlreadyExist.addAssiocatedPicture(id: iPicture._id)
                    _dataSaver.ModifyTag(iTag: tagAlreadyExist)
                    break
                }
            }
            if (!tagExist) {
                let lTag = Tag(name: tag)
                lTag.addAssiocatedPicture(id: iPicture._id)
                _dataSaver.SaveTags(iTag: lTag)
            }
        }
        
        iPicture._saved = true
        
        _dataSaver.SavePicture(iPicture: iPicture)
        
        ClearDatas()
        PullDatas()
    }
    
    // MARK: Modify a Picture
    func ModifyPicture(iPicture: Picture) -> Void {
        _dataSaver.ModifyPicture(iPicture: iPicture)
        ClearDatas()
        PullDatas()
    }
    
    // MARK: Delete all Pictures and Tags
    func Delete() {
        _dataSaver.Delete()
        ClearDatas()
        PullDatas()
    }
    
    // MARK: Delete a Picture.
    func DeletePicture(iPicture: Picture) {
        _dataSaver.DeletePicture(iPicture: iPicture)
        ClearDatas()
        PullDatas()
    }
    
    // MARK: Delete a Tag. Use it only if you want to delete a Tag. If you want to modify a tag from a picture, use ModifyPicture instead.
    func DeleteTag(iTag: Tag) {
        _dataSaver.DeleteTag(iTag: iTag)
        ClearDatas()
        PullDatas()
    }
    
}
