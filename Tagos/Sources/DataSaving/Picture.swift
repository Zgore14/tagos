//
//  Pictures.swift
//  Tagos
//
//  Created by Kilian Lebrun on 2018-11-12.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import Foundation
import UIKit

class Picture {
    var _image : UIImage
    var _dateOfCreation : Date
    var _tags : [String]
    var _id : UUID
    var _saved : Bool
    
    init() {
        _id = UUID()
        _image = UIImage()
        _dateOfCreation = Date()
        _tags = [String]()
        _saved = false
    }
    
    init(image: UIImage, tags: [String]) {
        _id = UUID()
        _image = image
        _tags = tags
        _dateOfCreation = Date()
        _saved = false
    }
}
