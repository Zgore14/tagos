//
//  MainCollectionViewCell.swift
//  Tagos
//
//  Created by Corentin Laithier on 2018-11-06.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
}
