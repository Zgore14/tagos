//
//  ViewController.swift
//  Tagos
//
//  Created by Corentin Laithier on 06/11/2018.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

struct cellObj {
    var label: String
    var picture: Picture
}

class MainViewController: UIViewController {
    
    let reuseCellIdentifier = "MainViewCell"
    var items: [cellObj] = []
    
    @IBOutlet weak var picCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GdataManager.dataManager.PullDatas()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        items = []
        let pictures = GdataManager.dataManager._pictures
        let tags = GdataManager.dataManager._tags
        
        // Do any additional setup after loading the view, typically from a nib.
        if (!pictures.isEmpty && !tags.isEmpty) {
            for tag in tags {
                let assocPicId = tag._assiocatedPicture.last!
                
                let pictureIdx = pictures.index(where: {
                    (elem) -> Bool in elem._id == assocPicId
                })
                items.append(cellObj(label: tag._name, picture: pictures[pictureIdx!]))
                self.picCollectionView.reloadData()
            }
        }
    }
    
    
    // MARK: Delete all the pictures
    @IBAction func DeleteDatas(_ sender: Any) {
        
        let alert = UIAlertController(title: "Alert", message: "Do you want to delete your library?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            switch action.style{
            case .default:
                GdataManager.dataManager.Delete()
                self.items = []
                self.picCollectionView.reloadData()
                break
            case .cancel:
                break
            case .destructive:
                break
            }}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AlbumViewController {
            if let indexPath = sender as? IndexPath {
                destination.label = self.items[indexPath.item].label
            }
        }
    }
    
}

// MARK: CollectionView Setup
extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // Tells the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // Make a cell for each element
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath as IndexPath) as! MainCollectionViewCell
        cell.imageView.image = self.items[indexPath.item].picture._image
        cell.imageView.layer.cornerRadius = 10.0
        cell.imageView.layer.masksToBounds = true
        cell.cellLabel.text = self.items[indexPath.item].label
        
        if indexPath.row % 2 == 0 {
            cell.leadingConstraint.constant = 10
            cell.trailingConstraint.constant = 5
        } else {
            cell.leadingConstraint.constant = 5
            cell.trailingConstraint.constant = 10
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You clicked on #\(indexPath)")
        performSegue(withIdentifier: "mainToAlbumSegue", sender: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width/2
        return CGSize(width: width, height: 1.2*width)
    }
    
    
}

