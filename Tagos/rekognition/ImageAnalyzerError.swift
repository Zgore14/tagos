//
//  TagosErrorDomain.swift
//  Tagos
//
//  Created by Daniel Ketterer on 09.11.18.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import Foundation

enum ImageAnalyzerError: Error {
    case unableToInitializeRequest
    case apiProblem(error: Error?)
}
