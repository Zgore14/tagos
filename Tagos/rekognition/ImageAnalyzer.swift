//
//  ImageAnalyzer.swift
//  Tagos
//
//  Created by Daniel Ketterer on 06.11.18.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import Foundation
import AWSRekognition
import PromiseKit

// MARK: - Singleton ImageAnalyzer
class ImageAnalyzer {
    
    // MARK: - Singleton setup
    static let instance = ImageAnalyzer()
    let rekognitionClient: AWSRekognition
    private init() {
        rekognitionClient = AWSRekognition.default()
    }
    
    // MARK: - Analyze an image with the AWS Rekognition API
    func analyzeImage(sourceImage: UIImage, minConfidence: Int) -> Promise<[String]> {
        return Promise { seal in
            let image = AWSRekognitionImage()
            
            // MARK: compress image
            image!.bytes = sourceImage.jpegData(compressionQuality: 0.5)
                        
            guard let request = AWSRekognitionDetectLabelsRequest() else {
                throw ImageAnalyzerError.unableToInitializeRequest
            }
            
            request.image = image
            request.minConfidence = NSNumber(value: minConfidence)
            
            var resultArray: [String] = []
            
            rekognitionClient.detectLabels(request){ (response:AWSRekognitionDetectLabelsResponse?, error:Error?) in
                
                // MARK: Callback with response from Amazon
                if error == nil {
                    for label in response?.labels ?? [] {
                        resultArray.append(label.name!)
                    }
                    seal.fulfill(resultArray)
                    
                } else {
                    seal.reject(ImageAnalyzerError.apiProblem(error: error))
                }
            }
        }
    }
}
