//
//  AppDelegate.swift
//  Tagos
//
//  Created by Corentin Laithier on 06/11/2018.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit
import AWSCore
import UserNotifications
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let credentialProvider = AWSStaticCredentialsProvider(accessKey: ProcessInfo.processInfo.environment["AWS_ACCESS_KEY"]!, secretKey:ProcessInfo.processInfo.environment["AWS_SECRET_KEY"]!)
        let configuration = AWSServiceConfiguration(region: .EUWest1, credentialsProvider: credentialProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let notificationCenter = UNUserNotificationCenter.current()
        // Request permission to display alerts and play sounds.
        notificationCenter.requestAuthorization(options: [.alert, .sound])
        { (granted, error) in
            if let error = error {
                print(error)
            }
            
            if (granted) {
                // MARK: - add push notification
                notificationCenter.getPendingNotificationRequests(completionHandler: {requests in
                    //check if already set
                    if (requests.filter { $0.trigger?.repeats ?? false }.count < 1) {
                        notificationCenter.getNotificationSettings { (settings) in
                            // Do not schedule notifications if not authorized.
                            guard settings.authorizationStatus == .authorized else {return}
                            
                            if settings.alertSetting == .enabled {
                                // Schedule an alert-only notification.
                                let content = UNMutableNotificationContent()
                                content.title = "Take pictures and tag them"
                                content.body = "You didn't use Tagos in a while, we miss you."
                                
                                // Configure the recurring date.
                                var dateComponents = DateComponents()
                                dateComponents.calendar = Calendar.current
                                
                                dateComponents.hour = 15
                                // sunday
                                dateComponents.weekday = 1
                                
                                // Create the trigger as a repeating event.
                                let trigger = UNCalendarNotificationTrigger(
                                    dateMatching: dateComponents, repeats: true)
                                
                                // Create the request
                                let uuidString = UUID().uuidString
                                let request = UNNotificationRequest(identifier: uuidString,
                                                                    content: content, trigger: trigger)
                                
                                // Schedule the request with the system.
                                notificationCenter.add(request) { (error) in
                                    if error != nil {
                                        print("unable to create notification:\n\(String(describing: error))")
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        let notificationCenter = UNUserNotificationCenter.current()
        // Request permission to display alerts and play sounds.
        notificationCenter.requestAuthorization(options: [.alert, .sound])
        { (granted, error) in
            if let error = error {
                print(error)
            }
            // MARK: - add push notification
            notificationCenter.getNotificationSettings { (settings) in
                // Do not schedule notifications if not authorized.
                guard settings.authorizationStatus == .authorized else {return}
                
                if settings.alertSetting == .enabled {
                    // Schedule an alert-only notification.
                    let content = UNMutableNotificationContent()
                    content.title = "Take pictures and tag them"
                    content.body = "You didn't use Tagos in a while, we miss you."

                    // Configure the recurring date.
                    var dateComponents = DateComponents()
                    dateComponents.calendar = Calendar.current
                    
                    let comp = Calendar.current.dateComponents([.hour, .minute, .weekday], from: Date())
                    let hour = comp.hour!
                    let minute = comp.minute!
                    let weekday = comp.weekday!
                    
                    dateComponents.minute = 1 + minute
                    dateComponents.hour = hour
                    dateComponents.weekday = weekday
                    
                    
                    // Create the trigger as a repeating event.
                    let trigger = UNCalendarNotificationTrigger(
                        dateMatching: dateComponents, repeats: false)
                    
                    // Create the request
                    let uuidString = UUID().uuidString
                    let request = UNNotificationRequest(identifier: uuidString,
                                                        content: content, trigger: trigger)
                    
                    // Schedule the request with the system.
                    notificationCenter.add(request) { (error) in
                        if error != nil {
                            print("unable to create notification:\n\(String(describing: error))")
                        }
                    }
                }
            }
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Saving_Data")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

