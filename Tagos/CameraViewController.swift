//
//  CameraViewController.swift
//  Tagos
//
//  Created by Pierre Zawadil on 2018-11-06.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit
import AVFoundation

// MARK: - The CameraViewController manage the session of the camera and manage captured images
class CameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var capturedImageView: UIImageView!
    @IBOutlet weak var takeButton: UIButton!
    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var discardButton: UIButton!
    @IBOutlet weak var switchButton: UIButton!
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var cameraIsUnavailable: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Shadow and Radius for takeButton
        takeButton.layer.shadowColor = UIColor.black.cgColor
        takeButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        takeButton.layer.masksToBounds = false
        takeButton.layer.shadowRadius = 1.0
        takeButton.layer.shadowOpacity = 0.5
        takeButton.layer.cornerRadius = takeButton.frame.width / 2
        // Configuration for validation buttons
        validateButton.isHidden = true
        discardButton.isHidden = true
        
        capturedImageView.image = nil
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if cameraIsUnavailable { return }
        self.captureSession.stopRunning()
        videoPreviewLayer.removeFromSuperlayer()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "validatePicture" {
            let dv = segue.destination as! DetailViewController
            dv.comeFromCamera = true
            dv.image = Picture(image: capturedImageView.image!, tags: [])
       }
    }

    // MARK: Picture taking
    @IBAction func didTakePhoto(_ sender: Any) {
        if self.cameraIsUnavailable { return }
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func discardCapturedImage(_ sender: Any) {
        capturedImageView.image = nil
        validateButton.isHidden = true
        discardButton.isHidden = true
        switchButton.isHidden = false
    }
    
    
    @IBAction func validateCapturedImage(_ sender: Any) {
        // Perform a segue to the image details view
        performSegue(withIdentifier: "validatePicture", sender: nil)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)
        capturedImageView.image = image
        validateButton.isHidden = false
        discardButton.isHidden = false
        switchButton.isHidden = true
    }
    
    // MARK: Camera live view
    @IBAction func switchCamera(_ sender: Any) {
        // TODO: Will be available in the new version of the camera view management
    }
    
    func setupLivePreview() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspect
        videoPreviewLayer.connection?.videoOrientation = .portrait
        cameraView.layer.insertSublayer(videoPreviewLayer, at: 0)
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.cameraView.bounds
            }
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .high
        
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                self.cameraIsUnavailable = true
                print("[ERROR] Unable to access back camera!")
                return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            self.cameraIsUnavailable = true
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
    }

}
