//
//  SearchViewController.swift
//  Tagos
//
//  Created by Corentin Laithier on 12/11/2018.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    let searchBar = UISearchBar()
    var pictures: [Picture] = []
    let reuseCellIdentifier = "SearchViewCellIdentifier"
    var tags: [Tag] = []
    var leftToDetail = false
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: Variables for the search function
    var filteredArray: [Picture] = []
    var shouldShowSearchResults = false
    
    override func viewWillAppear(_ animated: Bool) {
        
        pictures = GdataManager.dataManager._pictures
        tags = GdataManager.dataManager._tags
        if !leftToDetail {
            filteredArray = []
            searchBar.text = ""
            shouldShowSearchResults = false
        }
        self.collectionView.reloadData()
        leftToDetail = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GdataManager.dataManager.PullDatas()
        createSearchBar()
    }
    
    // MARK: Setup the search bar
    func createSearchBar(){
        searchBar.placeholder = "Search images"
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        self.navigationItem.titleView = searchBar
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = sender as? IndexPath {
                // TODO: change data structure and pass Picture object
                destination.image = self.pictures[indexPath.item]
                destination.imageTitle = "Image title"
                leftToDetail = true
            }
        }
    }
}

// MARK: Search feature
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredArray = []
        let filteredTags = tags.filter { tag in
            return tag._name.lowercased().contains(searchText.lowercased())
        }
        for tag in filteredTags {
            let candidates = pictures.filter { picture in
                return tag._assiocatedPicture.contains(picture._id)
            }
            for candidate in candidates {
                if !filteredArray.contains(where: { picture in
                    picture._id == candidate._id
                }) {
                    filteredArray.append(candidate)
                }
            }
        }
        
        
        if searchText != "" {
            shouldShowSearchResults = true
            self.collectionView.reloadData()
        } else {
            shouldShowSearchResults = false
            self.collectionView.reloadData()
        }
    }
}

// MARK: CollectionView to display the images
extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return self.filteredArray.count
        } else {
            return self.pictures.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath as IndexPath) as! AlbumViewCollectionViewCell
        
        if  shouldShowSearchResults {
            cell.imageView.image = self.filteredArray[indexPath.item]._image
        } else {
            cell.imageView.image = self.pictures[indexPath.item]._image
        }
        
        
        if indexPath.row % 3 == 0 {
            cell.leadingConstraint.constant = 0
            cell.trailingConstraint.constant = 1
        } else if indexPath.row % 3 == 1 {
            cell.leadingConstraint.constant = 0
            cell.trailingConstraint.constant = 1
        } else {
            cell.leadingConstraint.constant = 0
            cell.trailingConstraint.constant = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width/3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "searchToDetails", sender: indexPath)
    }
    
    // Hides the keyboard when scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    
    // Hides the keyboard when hit search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        shouldShowSearchResults = true
        self.collectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        shouldShowSearchResults = false
        self.collectionView.reloadData()
    }
    
}
