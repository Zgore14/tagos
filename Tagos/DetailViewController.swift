//
//  DetailViewController.swift
//  Tagos
//
//  Created by Daniel Ketterer on 12.11.18.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {


    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var bottomView: UIView!
    
    var imageTitle: String = ""
    var comeFromCamera: Bool = false
    var noTags: Bool = false
    var image: Picture = Picture()
    
    private var scrollView = ImageScrollView(image: UIImage())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView = ImageScrollView(image: image._image)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        navigationBar.title = dateFormatter.string(from: image._dateOfCreation)
        
        view.addSubview(scrollView)
        view.bringSubviewToFront(bottomView)
        
        scrollView.frame = view.frame
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(toggle))
        scrollView.addGestureRecognizer(gesture)
        scrollView.backgroundColor = UIColor(named: "white")

        addBorderToBottomView()
        
        if comeFromCamera {
            // start analyzing
            ImageAnalyzer.instance.analyzeImage(sourceImage: image._image, minConfidence: 70).done { resultArray in
                self.image._tags = resultArray
                self.collectionView.reloadData()
                
                print(resultArray)
                
                if (resultArray.isEmpty) {
                    self.noTags = true
                }
                
                
                GdataManager.dataManager.SavePicture(iPicture: self.image)
             
                // persist changes
                // if promise fullfills show labels in collection
                // set labels in tags array
                // retrigger view
                }.catch { error in
                    print(error)
                    self.noTags = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // hide tabbar
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // show tabbar
        tabBarController?.tabBar.isHidden = false
    }
    
    func addBorderToBottomView() {
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        // 5000 is wide enough, overstanding is clipped
        border.frame = CGRect(x: 0, y: 0, width: 5000, height: 0.5)
        bottomView.layer.addSublayer(border)
    }
    
    @objc func toggle() {
       // hide statusbar
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true)
        
        // hide bottom view
        bottomView.isHidden = bottomView.isHidden == false
        
        // toggle background of image background
        if (scrollView.backgroundColor == UIColor(named: "white")) {
            scrollView.backgroundColor = UIColor(named: "black")
        } else {
            scrollView.backgroundColor = UIColor(named: "white")
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    
    // MARK: - Navigation to EditTagsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navigationController = segue.destination as? UINavigationController {
            if let destination = navigationController.viewControllers.first as? EditTagsViewController {
                destination.tags = self.image._tags
                destination.callingController = self
            }
        }
    }
    
    func returnFromEdit(tags: [String]) {
        self.image._tags = tags
        if (noTags) {
            GdataManager.dataManager.SavePicture(iPicture: self.image)
        }
        else {
            GdataManager.dataManager.ModifyPicture(iPicture: self.image)
        }
        collectionView.reloadData()
    }
}

// MARK: - CollectionView
extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.image._tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as? TagCollectionViewCell {
            let label = self.image._tags[indexPath.row]
            cell.tagLabel.text = label
            
            cell.leadingConstraint.constant = 5
            cell.traillingConstraint.constant = 5
            if (indexPath.row == 0) {
                cell.leadingConstraint.constant = 20
            } else if (indexPath.row == (self.image._tags.count - 1)) {
                cell.traillingConstraint.constant = 20
            }
            
            return cell
        } else {
            return TagCollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let labelWidth = (self.image._tags[indexPath.row] as NSString).size(withAttributes: nil)
        var size = CGSize(width: labelWidth.width*1.5 + 10, height: collectionView.frame.height)
        if (indexPath.row == 0 || indexPath.row == (self.image._tags.count - 1)) {
            size.width += 15
        }
        return size
    }
 
}
