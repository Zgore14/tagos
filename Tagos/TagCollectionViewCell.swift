//
//  TagCollectionViewCell.swift
//  Tagos
//
//  Created by Daniel Ketterer on 12.11.18.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var traillingConstraint: NSLayoutConstraint!
}
