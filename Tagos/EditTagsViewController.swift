//
//  EditTagsViewController.swift
//  Tagos
//
//  Created by Daniel Ketterer on 12.11.18.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class EditTagsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var tags: [String] = []
    var callingController: DetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.setEditing(true, animated: true)
        
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: Any) {
        // TODO: find the right controller, presenting is the navigation
        if let presenter = callingController {
            dismiss(animated: true, completion: {
                presenter.returnFromEdit(tags: self.tags)
            })
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateTable()
        // hide tabbar
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // show tabbar
        tabBarController?.tabBar.isHidden = false
    }
}

// MARK: - TableView Delegate & DataSource
extension EditTagsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return tags.count
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(indexPath.section) {
        case 0:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = tags[indexPath.row]
            return cell
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NewTagTableViewCell", for: indexPath) as? NewTagTableViewCell {
                return cell
            } else {
                return UITableViewCell()
            }
           
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if (indexPath.section == 0) {
            return .delete
        } else {
            return .insert
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tags.remove(at: indexPath.row)
            //tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        } else if editingStyle == .insert {
            if let cell = tableView.cellForRow(at: indexPath) as? NewTagTableViewCell {
                if let text = cell.textField.text {
                    if (text.count > 1 && text.count < 50) {
                        tags.append(text)
                        cell.textField.text = ""
                        tableView.reloadData()
                    }
                }
            }
            
        }
    }
    
    
    // MARK: Table animation
    func animateTable(){
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableViewHeight = tableView.bounds.height
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        
        for cell in cells {
            UIView.animate(withDuration: 1.75, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
    
    
}
