//
//  AlbumViewCollectionViewCell.swift
//  Tagos
//
//  Created by Corentin Laithier on 12/11/2018.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class AlbumViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!    
    
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
}
