//
//  AlbumViewController.swift
//  Tagos
//
//  Created by Corentin Laithier on 2018-11-06.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class AlbumViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    var label = ""
    let reuseCellIdentifier = "AlbumViewCellIdentifier"
    
    var pictures: [Picture] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.title = label
        
        let allPictures = GdataManager.dataManager._pictures
        
        for picture in allPictures {
            if picture._tags.first(where: {$0 == label}) != nil {
                self.pictures.append(picture)
            }
        }
}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = sender as? IndexPath {
                destination.image = self.pictures[indexPath.item]
                destination.imageTitle = ""
            }
        }
    }
    

}

// MARK: CollectionView Setup
extension AlbumViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath as IndexPath) as! AlbumViewCollectionViewCell
        cell.imageView.image = self.pictures[indexPath.item]._image
        
        if indexPath.row % 3 == 0 {
            cell.leadingConstraint.constant = 0
            cell.trailingConstraint.constant = 1
        } else if indexPath.row % 3 == 1 {
            cell.leadingConstraint.constant = 0
            cell.trailingConstraint.constant = 1
        } else {
            cell.leadingConstraint.constant = 0
            cell.trailingConstraint.constant = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width/3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toDetailsView", sender: indexPath)
    }
    
    
}
