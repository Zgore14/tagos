//
//  NewTagTableViewCell.swift
//  Tagos
//
//  Created by Daniel Ketterer on 12.11.18.
//  Copyright © 2018 Corentin Laithier. All rights reserved.
//

import UIKit

class NewTagTableViewCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
