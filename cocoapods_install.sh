#!/bin/bash

## Need to be executed at the root of Tagos Project directory.
## Script made for JU PC, in order to install the cacaopods library without the root access.

GEM_HOME=$HOME/.gem gem install cocoapods
export PATH=$PATH:$HOME/.gem/bin
GEM_HOME=$HOME/.gem pod setup
GEM_HOME=$HOME/.gem pod install
