# TagOs


## Project description

The purpose of _TagOs_ is to find, sort and organize your pictures with automatically determined tags.
Pictures can be taken directly on the app, and tags can be added manually as well.

The app uses the _AWS Rekognition API_ to classify the pictures. When the user takes a photo and switches to the detail view, the app makes a request to the API. The request contains the image. The response has a list of labels for the image. Each label has a confidence value, called score. The app sets some threshold which labels above a confidence should be kept or discarded.



## Mockup details

### Camera View

The user takes a new photo with the _Camera View_. She can use the front or the back camera for that. After a photo is taken, she decides whether to discard it or continue to the detail picture view by confirming the photo. When the photo is taken, the user is then taken to the _Detail Picture View_ where the photo he just took is displayed.

### Main View

The _Main View_ is where the user comes first. All the photos are regrouped in _albums_ that are actually corresponding to a tag name. So, a single photo can be in multiple albums. Each album is represented by the name of the associated tag, and a preview of the last picture taken that contains this tag. From this page, the user can swipe to the left or click on the camera icon to go to the camera view and take pictures. He can also use the plus sign to import picture from his photo library.

### Album View

_Album View_’s goal is to display all pictures related to a tag in an asymmetric grid, it’s possible to click on a picture to see its details. Therefore, users can add tags into the search bar to display pictures with more than one tag. Searched tags will be displayed under the search bar, and user can also remove tags that he searched.

### Details Picture View

The _Details Picture View_ is the view that will display the selected picture and the associated tags. The user can either add or remove tags from this view. It is possible to go back to the album view.

## Setup for AWS

To build the project with the AWS SDK dependency you have to [install](https://cocoapods.org/) cocoapods first.  
Then run `pod setup` and `pod install` in the project folder (the one contain the folder calles tagos and the .xcodeproj file).

After the cocoapods setup close Xcode and open it again using the xcworkspace file in the project folder. Always open the project that way to make sure you can built it using the dependencies.

Though the source code is public, the AWS credentials are not included in the source code. Use environmental variables in Xcode. Open Product -> Schema -> Edit Schema. Add the variables `AWS_ACCESS_KEY` and `AWS_SECRET_KEY`. Untick the "shared" option on the bottom of the window, otherwise it will be commited in an other file.



## Credits

<div>Icons made by <a href="https://www.flaticon.com/authors/hadrien" title="Hadrien">Hadrien</a>, <a href="https://www.flaticon.com/authors/gregor-cresnar" title="Gregor Cresnar">Gregor Cresnar</a>, <a href="https://www.flaticon.com/authors/srip" title="srip">srip</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
